Functional requirements:
- Initialize the board (4 pieces in every pit)
- Have instructions at beginning of game.
- Draw the board and game pieces
- Simulate dropping of pieces around the board, according to game rules and update the game board.
- Check the board after a player's turn to identify:
    - Did someone win?
    - If last piece was dropped in store, player goes again. otherwise, swap players.
    - If last piece dropped in empty pit on player's on side, move all pieces in pit and opposite side into the player's store.
- Allow 2nd player. This includes swapping players after each turn.
- Process input from keyboard or mouse to select starting pit when it's a player's turn.
- When either player has 0 pieces remaining in pits, end game. Determine winner (count remaining pieces on players' sides + stores).
- Display score on screen after player wins

Optional requirements:
- Create a bot to play or online multiplayer

Implementation Steps:
1. Determine data structure
2. Draw initial shape of board, print number in each pit as text
3. Implement game logic
  - Simulate gameplay
  - Determine winner
4. Update graphics (draw pieces)
5. Add flair - instructions, display score, etc.