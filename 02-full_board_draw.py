# Simple pygame program

# Configuration
NUM_PITS = 6    # Defines number of pits on board per player

# Colors
BOARD_BACKGROUND_RGB = (193,154,107)
PIT_RGB = (143,114,79)

# Dimensions/positions
SCREEN_WIDTH_PX = 1280
SCREEN_HEIGHT_PX = 720
LEFT_MARGIN_PX = 200
PIT_WIDTH_PX = 100
PIT_PADDING_PX = 10

# Calcualted constants
TOP_MARGIN_PX = SCREEN_HEIGHT_PX / 2 - (PIT_PADDING_PX * 1.5 + PIT_WIDTH_PX)
PIT_RADIUS_PX = PIT_WIDTH_PX / 2
BOARD_WIDTH_PX = PIT_PADDING_PX + (NUM_PITS + 2) * (PIT_PADDING_PX + PIT_WIDTH_PX)
BOARD_HEIGHT_PX = PIT_PADDING_PX + 2 * (PIT_PADDING_PX + PIT_WIDTH_PX)

################################################
# Game variables
################################################

# Board consists of 2 arrays of length 7. Each array represents each player's side.
# First 6 elements are the player's pits, last element is the player's store
# 
#        idx:   0   1   2   3   4   5   6   7
#   Player 1:   4   4   4   4   4   4   4   0 
#   Player 2:   4   4   4   4   4   4   4   0
#               ========== pits =========   ^store
board = [[4]*NUM_PITS + [0] for player in range(2)]

currPlayerNum = 1   # Either 1 or 2, representing player number

# Function to draw a player's store, given the CENTER coordinate
def drawStore(centerX, centerY):
    pygame.draw.circle(screen, PIT_RGB, (centerX, centerY - (PIT_PADDING_PX + PIT_RADIUS_PX)), PIT_RADIUS_PX)
    pygame.draw.rect(screen, PIT_RGB, (centerX - PIT_RADIUS_PX, centerY - (PIT_PADDING_PX + PIT_RADIUS_PX), PIT_WIDTH_PX, PIT_WIDTH_PX + 2 * PIT_PADDING_PX)) 
    pygame.draw.circle(screen, PIT_RGB, (centerX, centerY + (PIT_PADDING_PX + PIT_RADIUS_PX)), PIT_RADIUS_PX)

# Function to draw half of mancala board. Modify referenceCorner (top-left or bottom-right coordinates as tuple) and xyFactor (1 or -1) to 
def drawHalfBoard(referenceCorner, xyFactor):
    for c in range(NUM_PITS):
        pitXFromCorner = PIT_RADIUS_PX + PIT_PADDING_PX + (1 + c) * (PIT_WIDTH_PX + PIT_PADDING_PX) # Margin + space for opponent store + pits to left
        pitYFromCorner = BOARD_HEIGHT_PX / 2 + PIT_PADDING_PX + PIT_RADIUS_PX    # Half screen + padding + 1 radius
        pitX = referenceCorner[0] + xyFactor * pitXFromCorner
        pitY = referenceCorner[1] + xyFactor * pitYFromCorner
        pygame.draw.circle(screen, PIT_RGB, (pitX, pitY), PIT_RADIUS_PX)

    # Draw store
    pitXFromCorner = PIT_RADIUS_PX + PIT_PADDING_PX + (1 + NUM_PITS) * (PIT_WIDTH_PX + PIT_PADDING_PX) # Margin + space for opponent store + pits to left
    pitYFromCorner = BOARD_HEIGHT_PX / 2
    pitX = referenceCorner[0] + xyFactor * pitXFromCorner
    pitY = referenceCorner[1] + xyFactor * pitYFromCorner
    drawStore(pitX, pitY)

################################################
# Main program
################################################

# Import and initialize the pygame library
import pygame
pygame.init()

# Set up the drawing window
screen = pygame.display.set_mode([SCREEN_WIDTH_PX, SCREEN_HEIGHT_PX])

# Run until the user asks to quit
running = True
while running:

    # Did the user click the window close button?
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # Fill the background with white
    screen.fill((20, 20, 20))

    # Draw board dimensions
    pygame.draw.rect(screen, BOARD_BACKGROUND_RGB, (LEFT_MARGIN_PX, TOP_MARGIN_PX, BOARD_WIDTH_PX, BOARD_HEIGHT_PX))

    # Draw pits
    drawHalfBoard((LEFT_MARGIN_PX, TOP_MARGIN_PX), 1)
    drawHalfBoard((LEFT_MARGIN_PX + BOARD_WIDTH_PX, TOP_MARGIN_PX + BOARD_HEIGHT_PX), -1)

    # Flip the display
    pygame.display.flip()

# Done! Time to quit.
pygame.quit()